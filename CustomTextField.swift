//
//  CustomTextField.swift
//  CustomTextFieldFramework
//
//  Created by Euler Carvalho on 13/06/18.
//  Copyright © 2018 Euler Carvalho. All rights reserved.
//
// NOTE: Using SwiftMaskField Pod, keep reading, for more informations.
//  SwiftMaskField.swift
//  GitHub: https://github.com/moraisandre/SwiftMaskText
//
//  Created by Andre Morais on 3/9/16.
//  Translated to Swift 3 by: André Santana Ferreira on 31/5/17
//  Copyright © 2016 Andre Morais. All rights reserved.
//  Website: http://www.andremorais.com.br
//

import UIKit

enum InputType: String {
    case phone
    case cpf
    case cnpj
    case email
    case notSet
    
    var maskField: String {
        switch self {
        case .phone:
            return "(NN) NNNNN-NNNN"
        case .cpf:
            return "NNN.NNN.NNN-NN"
        case .cnpj:
            return "NN.NNN.NNN/NNNN-NN"
        case .email:
            return "*@*.*"
        case .notSet:
            return "*"
        }
    }
    
    static let allValues: [InputType] = [.phone, .cpf, .cnpj, .email, .notSet]
    
    static func inputType(for text: String) -> InputType? {
        for input in InputType.allValues {
            if text.lowercased().contains(input.rawValue) {
                return input
            }
        }
        return .notSet
    }
}

public class CustomTextField: UITextField {
    
    //    var textFieldDelegate: TextFieldDelegate?
    
    var lineLayer: CALayer!
    private var _mask: String!
    /*
     // Only override draw() if you perform custom drawing.
     // An empty implementation adversely affects performance during animation.
     override func draw(_ rect: CGRect) {
     // Drawing code
     }
     */
    
    @IBInspectable public var maskString: String {
        
        get {
            return _mask
        }
        
        set {
            _mask = newValue
        }
        
    }
    
    var inputType: InputType = .notSet {
        didSet {
            self.maskString = inputType.maskField
        }
    }
    
    override public var isHighlighted: Bool {
        didSet {
            lineLayer.backgroundColor = (isHighlighted ? UIColor.green : UIColor.gray).cgColor
        }
    }
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        initialize()
//        self.addDoneButtonOnKeyboard()
    }
    
    override public func layoutSubviews() {
        super.layoutSubviews()
        layoutLineLayer()
    }
    
    fileprivate func initialize() {
        textColor = UIColor.gray
        borderStyle = .none
        tintColor = UIColor.green
        
        setupLineLayer()
        addTarget(self, action: #selector(CustomTextField.beginEditingHandler(textField:)), for: .editingDidBegin)
        addTarget(self, action: #selector(CustomTextField.endEditingHandler(textField:)), for: .editingDidEnd)
        addTarget(self, action: #selector(CustomTextField.textDidChange(textField:)), for: .editingChanged)
        self.autocorrectionType = .no
    }
    
    private func setupLineLayer() {
        lineLayer = CALayer()
        lineLayer.backgroundColor = UIColor.gray.cgColor
        layer.addSublayer(lineLayer)
        
    }
    
    func layoutLineLayer() {
        lineLayer.frame = CGRect(origin: CGPoint(x: 0, y: frame.size.height - 2.0),
                                 size: CGSize(width: frame.size.width, height: 2.0))
    }
    
    @objc private func beginEditingHandler(textField: UITextField) {
        lineLayer.backgroundColor = UIColor.green.cgColor
    }
    
    @objc private func endEditingHandler(textField: UITextField) {
        lineLayer.backgroundColor = UIColor.gray.cgColor
    }
    
    @objc private func textDidChange(textField: UITextField) {
        applyFilter(textField: textField)
    }
    
    public func applyFilter(textField: UITextField) {
        if _mask == nil {
            return
        }
        
        var index = _mask.startIndex
        var textWithMask: String = ""
        var i: Int = 0
        var text: String = textField.text!
        
        if (text.isEmpty) {
            return
        }
        
        text = removeMaskCharacters(text: text, withMask: maskString)
        
        while(index != maskString.endIndex) {
            
            if(i >= text.count) {
                self.text = textWithMask
                break
            }
            
//            if("\(maskString[index])" == "N") { //Only number
//                if (!isNumber(textToValidate: text[i])) {
//                    break
//                }
//                textWithMask = textWithMask + text[i]
//                i += 1
//            } else if("\(maskString[index])" == "C") { //Only Characters A-Z, Upper case only
//                if(hasSpecialCharacter(searchTerm: text[i])) {
//                    break
//                }
//
//                if (isNumber(textToValidate: text[i])) {
//                    break
//                }
//                textWithMask = textWithMask + text[i].uppercased()
//                i += 1
//            } else if("\(maskString[index])" == "c") { //Only Characters a-z, lower case only
//                if(hasSpecialCharacter(searchTerm: text[i])) {
//                    break
//                }
//
//                if (isNumber(textToValidate: text[i])) {
//                    break
//                }
//                textWithMask = textWithMask + text[i].lowercased()
//                i += 1
//            } else if("\(maskString[index])" == "X") { //Only Characters a-Z
//                if(hasSpecialCharacter(searchTerm: text[i])) {
//                    break
//                }
//
//                if (isNumber(textToValidate: text[i])) {
//                    break
//                }
//                textWithMask = textWithMask + text[i]
//                i += 1
//            } else if("\(maskString[index])" == "%") { //Characters a-Z + Numbers
//                if(hasSpecialCharacter(searchTerm: text[i])) {
//                    break
//                }
//                textWithMask = textWithMask + text[i]
//                i += 1
//            } else if("\(maskString[index])" == "U") { //Only Characters A-Z + Numbers, Upper case only
//                if(hasSpecialCharacter(searchTerm: text[i])) {
//                    break
//                }
//
//                textWithMask = textWithMask + text[i].uppercased()
//                i += 1
//            } else if("\(maskString[index])" == "u") { //Only Characters a-z + Numbers, lower case only
//                if(hasSpecialCharacter(searchTerm: text[i])) {
//                    break
//                }
//
//                textWithMask = textWithMask + text[i].lowercased()
//                i += 1
//            } else if("\(maskString[index])" == "*") { //Any Character
//                textWithMask = textWithMask + text[i]
//                i += 1
//            } else {
//                textWithMask = textWithMask + "\(maskString[index])"
//            }
            
            index = _mask.index(after: index)
        }
        
        self.text = textWithMask
    }
    
    public func isNumber(textToValidate: String) -> Bool {
        
        let num = Int(textToValidate)
        
        if num != nil {
            return true
        }
        
        return false
    }
    
    public func hasSpecialCharacter(searchTerm: String) -> Bool {
        let regex = try!  NSRegularExpression(pattern: ".*[^A-Za-z0-9].*", options: NSRegularExpression.Options())
        
        if regex.firstMatch(in: searchTerm, options: NSRegularExpression.MatchingOptions(), range: NSMakeRange(0, searchTerm.count)) != nil {
            return true
        }
        
        return false
        
    }
    
    public func removeMaskCharacters( text: String, withMask mask: String) -> String {
        
        var mask = mask
        var text = text
        mask = mask.replacingOccurrences(of: "X", with: "")
        mask = mask.replacingOccurrences(of: "N", with: "")
        mask = mask.replacingOccurrences(of: "C", with: "")
        mask = mask.replacingOccurrences(of: "c", with: "")
        mask = mask.replacingOccurrences(of: "U", with: "")
        mask = mask.replacingOccurrences(of: "u", with: "")
        mask = mask.replacingOccurrences(of: "*", with: "")
        
        var index = mask.startIndex
        
        while(index != mask.endIndex) {
            text = text.replacingOccurrences(of: "\(mask[index])", with: "")
            index = mask.index(after: index)
        }
        
        return text
    }
}

public class SecureTextField: CustomTextField {
    
    var toogleButton: UIButton!
    
    override func initialize() {
        super.initialize()
        toogleSecure()
        isSecureTextEntry = true
    }
    
    private func toogleSecure() {
        toogleButton = UIButton(frame: CGRect(x: 0, y: 0, width: 40, height: 20))
        
        toogleButton.addTarget(self, action: #selector(toogleSecureText), for: .touchUpInside)
        
        toogleButton.setImage(#imageLiteral(resourceName: "Ver off"), for: .selected)
        toogleButton.setImage(#imageLiteral(resourceName: "Ver off"), for: .normal)
        
        toogleButton.contentMode = .center
        rightView = toogleButton
        rightViewMode = .always
    }
    
    @objc private func toogleSecureText() {
        self.isSecureTextEntry = !self.isSecureTextEntry
        toogleButton.setImage(self.isSecureTextEntry ? #imageLiteral(resourceName: "Ver off") : #imageLiteral(resourceName: "Ver on"), for: .selected)
        toogleButton.setImage(self.isSecureTextEntry ? #imageLiteral(resourceName: "Ver off") : #imageLiteral(resourceName: "Ver on"), for: .normal)
        
        if toogleButton.isSelected {
            toogleButton.isSelected = false
        } else {
            toogleButton.isSelected = true
        }
    }
}

public extension CustomTextField {
    override public func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
    }
}

public extension CustomTextField {
    func shake() {
        let animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: kCAMediaTimingFunctionLinear)
        animation.duration = 0.6
        animation.values = [-15.0, 15.0, -15.0, 15.0, -10.0, 10.0, -5.0, 5.0, 0.0 ]
        
        self.layer.add(animation, forKey: "shake")
    }
    
    func changeLineColor(to color: CGColor = UIColor.red.cgColor) {
        self.lineLayer.backgroundColor = color
    }
    
    func showError() {
        shake()
        changeLineColor()
    }
}
