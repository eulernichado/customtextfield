# ECTextField

[![CI Status](https://img.shields.io/travis/Euler/ECTextField.svg?style=flat)](https://travis-ci.org/Euler/ECTextField)
[![Version](https://img.shields.io/cocoapods/v/ECTextField.svg?style=flat)](https://cocoapods.org/pods/ECTextField)
[![License](https://img.shields.io/cocoapods/l/ECTextField.svg?style=flat)](https://cocoapods.org/pods/ECTextField)
[![Platform](https://img.shields.io/cocoapods/p/ECTextField.svg?style=flat)](https://cocoapods.org/pods/ECTextField)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

ECTextField is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'ECTextField'
```

## Author

Euler, euler.nichado@gmail.com

## License

ECTextField is available under the MIT license. See the LICENSE file for more info.
